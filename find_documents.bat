@echo off

if "%1"=="" goto start_c
echo -------------------HWP----------------------->>..\file_repo\\search.txt
forfiles /m *.hwp /C "cmd /c echo @path" /s /p %1>>..\file_repo\\search.txt
echo --------------------------------------------->>..\file_repo\\search.txt
echo -------------------DOC----------------------->>..\file_repo\\search.txt
forfiles /m *.doc /C "cmd /c echo @path" /s /p %1>>..\file_repo\\search.txt
echo --------------------------------------------->>..\file_repo\\search.txt
echo -------------------DOCX---------------------->>..\file_repo\\search.txt
forfiles /m *.docx /C "cmd /c echo @path" /s /p %1>>..\file_repo\\search.txt
echo --------------------------------------------->>..\file_repo\\search.txt
goto EOF

:start_c
call %0 C:\

:EOF